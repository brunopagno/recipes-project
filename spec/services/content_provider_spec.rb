# frozen_string_literal: true

require_relative '../../app/services/content_provider'
require_relative '../../app/errors'
require 'vcr'

describe Services::ContentProvider do
  subject { Services::ContentProvider }

  describe 'client' do
    describe 'fetch_content' do
      context 'when requesting recipes' do
        it 'returns list of recipes' do
          VCR.use_cassette 'list_recipes' do
            result = subject.fetch_content('recipe')
            expect(result.length).to eq(4)
            expect(result[0].title).to eq('White Cheddar Grilled Cheese with Cherry Preserves & Basil')
            expect(result[1].title).to eq('Tofu Saag Paneer with Buttery Toasted Pita')
            expect(result[2].title).to eq('Grilled Steak & Vegetables with Cilantro-Jalapeño Dressing')
            expect(result[3].title).to eq("Crispy Chicken and Rice\twith Peas & Arugula Salad")
          end
        end
      end

      context 'when requesting something that content provider does not understand' do
        it 'raises exception' do
          VCR.use_cassette 'list_anything' do
            expect { subject.fetch_content('anything') }.to raise_error(InvalidContentError)
          end
        end
      end
    end

    describe 'fetch_single' do
      context 'when requesting content that exists' do
        it 'returns content' do
          VCR.use_cassette 'single_content' do
            id = '4dT8tcb6ukGSIg2YyuGEOm'
            result = subject.fetch_single(id)
            expect(result.title).to eq('White Cheddar Grilled Cheese with Cherry Preserves & Basil')
          end
        end
      end

      context 'when requesting unknown id' do
        it 'raises exception' do
          VCR.use_cassette 'single_not_found' do
            expect { subject.fetch_single('not_found') }.to raise_error(ContentNotFoundError)
          end
        end
      end
    end
  end
end
