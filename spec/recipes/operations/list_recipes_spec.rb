# frozen_string_literal: true

require_relative '../../../app/recipes/operations/list_recipes'
require_relative '../../../app/services/content_provider'

describe 'ListRecipes' do
  subject { Operations::ListRecipes }
  let(:recipes) { [{ id: 1, title: 'Recipe 1' }] }

  before do
    allow(Services::ContentProvider).to receive('fetch_content').with('recipe').and_return(recipes)
  end

  context 'when api returns list of recipes' do
    it 'returns a list of recipes' do
      result = subject.call
      expect(result).to eq(recipes)
    end
  end
end
