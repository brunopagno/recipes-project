# frozen_string_literal: true

require_relative '../../../app/recipes/operations/fetch_recipe'
require_relative '../../../app/services/content_provider'

describe 'FetchRecipe' do
  subject { Operations::FetchRecipe }
  let(:recipe) { { id: 1, title: 'Recipe 1' } }

  before do
    allow(Services::ContentProvider).to receive('fetch_single').with(1).and_return(recipe)
  end

  context 'when api returns list of recipes' do
    it 'returns a list of recipes' do
      id = 1

      result = subject.call(id)

      expect(result).to eq(recipe)
    end
  end
end
