# frozen_string_literal: true

require 'pathname'

ROOT_PATH = Pathname.new(File.dirname(__FILE__))
$LOAD_PATH << ROOT_PATH.join('app')

require 'dotenv/load'
require 'recipes/recipes_api'
require 'byebug'
