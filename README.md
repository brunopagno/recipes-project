# Recipes API

Hello reviewer :)
\
This project is my take on solving the proposed challenge.

## How to run the project

If you're using docker then simply run the following command at the root of the directory
```
$ docker-compose up
```

Otherwise you can run by using the following lines at the root of the project. You will need to have ruby installed (preferentially version 2.7.2)
```
$ bundle install
$ rackup config.ru -o 0.0.0.0 -p 8080 -s thin
```

And then go to http://localhost:8080/recipes
## About the solution

The first thing you'll see is the folder structure.

* **app**: the whole application
  * **recipes**: everything related to recipes
    * **operations**: operations (or actions) that controllers use
    * **static**: any public/static files
    * **views**: views (erb/html)
  * **services**: communication with external systems/api
  * **errors**: internal errors
* **spec**: tests
* **vcr_cassettes**: part of the `vcr` gem and a bit better than mocking

I tried to keep everything separated by concerns. This allows for a very streamlined development with clear separation between different parts of the application. So for example if we wanted to add something that is not related to recipes like user account, we could create another folder `users` under app and keep everything isolated properly.

Additionally I decided to re-raise services errors with internally defined errors. It does not make much difference in a project this size, but I believe that internal systems (files inside recipes folder) should not need to deal with external types of errors.

Finally for testing an external API I could either mock everything or use some logic to make the request simpler. So I'm using vcr gem. It still is not perfect, as if the API changes the tests do not break, but it is still better than mocking IMHO.
## About decisions and development

I chose to use Sinatra for the challenge for two reasons. First because the end result looked a bit simple for using Rails ("one should not use a Bazooka to kill a fly") and we don't even need a DB for the project. Second because it had been quite a while that I played with Sinatra, so it was nice to get back to it.

During development things went reasonably well, but I had one tiny problem where I accidentally commited SpaceID and Access Token to the repo because of the VCR_Cassettes. So I decided to simply restart the repository instead of trying to adjust the commits. It felt like the extra effort was not necessary.

Lastly, I preferred not to use my current work computer and did this project on my Windows machine using WSL. It's not the best experience but I believe I got everything working. Just letting you know in case something obvious is off when testing on another OS.

---

Thanks a lot for the opportunity.