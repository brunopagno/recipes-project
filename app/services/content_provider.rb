# frozen_string_literal: true

require 'contentful'
require_relative '../errors'

SPACE_ID = ENV['SPACE_ID']
ACCESS_TOKEN = ENV['ACCESS_TOKEN']

module Services
  class ContentProvider
    def self.fetch_content(content_type)
      client.entries(content_type: content_type)
    rescue Contentful::BadRequest
      raise InvalidContentError
    end

    def self.fetch_single(id)
      client.entry(id)
    rescue Contentful::NotFound
      raise ContentNotFoundError
    end

    def self.client
      @client ||= Contentful::Client.new(
        space: SPACE_ID,
        access_token: ACCESS_TOKEN
      )
    end
  end
end
