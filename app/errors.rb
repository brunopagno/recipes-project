# frozen_string_literal: true

class ContentNotFoundError < StandardError
  def message
    'Content was not found'
  end
end

class InvalidContentError < StandardError
  def message
    'Content type is not valid'
  end
end
