# frozen_string_literal: true

require 'sinatra/base'
require 'recipes/operations/list_recipes'
require 'recipes/operations/fetch_recipe'

class RecipesApi < Sinatra::Base
  set :views, "#{settings.root}/views"
  set :public_folder, "#{settings.root}/static"

  get '/recipes' do
    raw = Operations::ListRecipes.call
    recipes = raw.map do |recipe|
      # in theory we could expand this as a decorator class which takes recipes,
      # but for simplicity sake let's use OpenStruct
      OpenStruct.new({
                       id: recipe.id,
                       title: recipe.title,
                       image: recipe.photo.url
                     })
    end
    erb :list_recipes, locals: { recipes: recipes }
  end

  get '/recipes/:id' do
    raw = Operations::FetchRecipe.call(params[:id])
    recipe = OpenStruct.new({
                              id: raw.id,
                              title: raw.title,
                              description: raw.description,
                              image: raw.photo.url,
                              tags: raw.respond_to?('tags') ? raw.tags.map(&:name) : [],
                              chef: raw.respond_to?('chef') ? raw.chef.name : nil
                            })
    erb :recipe, locals: { recipe: recipe }
  end
end
