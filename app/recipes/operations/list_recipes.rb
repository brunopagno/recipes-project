# frozen_string_literal: true

require_relative '../../services/content_provider'

module Operations
  class ListRecipes
    def self.call
      Services::ContentProvider.fetch_content('recipe')
    end
  end
end
