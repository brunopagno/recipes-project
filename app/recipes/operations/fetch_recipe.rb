# frozen_string_literal: true

require_relative '../../services/content_provider'

module Operations
  class FetchRecipe
    def self.call(id)
      Services::ContentProvider.fetch_single(id)
    end
  end
end
